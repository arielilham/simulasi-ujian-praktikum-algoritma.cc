#include <iostream>
using namespace std;
int main()
{
    int i, j, pertambahan, pengurangan, perkalian, modulus, pembagian;
    cout << "Masukan Nilai i : ";
    cin >> i;
    cout << "Masukan Nilai j : ";
    cin >> j;
    pertambahan = i + j;
    pengurangan = i - j;
    perkalian = i * j;
    pembagian = i / j;
    modulus = i % j;
    cout << "---------------------------" << endl;
    cout << "| operasi | hasil operasi |" << endl;
    cout << "---------------------------" << endl;
    cout << "|  " << i << " + " << j << "  |\t " << pertambahan << "\t  |" << endl;
    cout << "|  " << i << " - " << j << "  |\t " << pengurangan << "\t  |" << endl;
    cout << "|  " << i << " x " << j << "  |\t " << perkalian << "\t  |" << endl;
    cout << "|  " << i << " : " << j << "  |\t " << pembagian << "\t  |" << endl;
    cout << "|  " << i << " % " << j << "  |\t " << modulus << "\t  |" << endl;
    cout << "---------------------------";
}





