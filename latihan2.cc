#include <iostream>
using namespace std;

int main() {
    string nama_karyawan;
    char golongan;
    string status;

    cout << "Nama Karyawan: ";
    cin >> nama_karyawan;
    cout << "Golongan (A/B): ";
    cin >> golongan;
    cout << "Status (Nikah/Belum): ";
    cin >> status;

    int gaji_pokok, tunjangan;
    double prosentase_iuran, potongan_iuran, gaji_bersih;

    if (golongan == 'A') {
        gaji_pokok = 200000;
        if (status == "Nikah") {
            tunjangan = 50000;
        } else {
            tunjangan = 25000;
        }
    } else if (golongan == 'B') {
        gaji_pokok = 350000;
        if (status == "Nikah") {
            tunjangan = 75000;
        } else {
            tunjangan = 60000;
        }
    } else {
        cout << "Golongan tidak valid" << endl;
        return 1;
    }

    if (gaji_pokok + tunjangan <= 300000) {
        prosentase_iuran = 0.05;
    } else {
        prosentase_iuran = 0.10;
    }

    potongan_iuran = (gaji_pokok + tunjangan) * prosentase_iuran;
    gaji_bersih = gaji_pokok + tunjangan - potongan_iuran;

    cout << "Gaji Pokok : Rp. " << gaji_pokok << endl;
    cout << "Tunjangan : Rp. " << tunjangan << endl;
    cout << "Potongan Iuran: Rp. " << potongan_iuran << endl;
    cout << "Gaji Bersih : Rp. " << gaji_bersih << endl;

    return 0;
}